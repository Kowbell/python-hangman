import random

wordsFile     = open("Words.txt",'r')                         # Opening our file of words
hangmanFile   = open("Hangman.txt",'r')                       # Opening our file of hangman images
wordString    = wordsFile.read()                              # Reading our file of words
hangmanString = hangmanFile.read()                            # Reading our file of hangman images

words         = wordString.split(',')                         # A list of words
word          = words[random.randint(0,len(words)-1)].lower() # The word to guess
hangmans      = hangmanString.split('.')                      # Splitting the hangman images up
guesses       = []                                            # Letters guessed
wordGuessed   = "_"*len(word)                                 # String of guessed word
guessesWrong  = 0                                             # How many wrong guesses
letters       = letters = list(set(word))                     # Letters in the word

def Intro():
    print "\n\n  Welcome to Hangman!\nTry to guess the word without dying!\nNow with 100% more 14th century Catholicism!\n"
    # Yeah, that's right. I did a one liner with escape sequences instead of a multi-lined string.
    
def GameOver(bool, word, guesses):
    if bool:
        print "\nYou won!\nThe word was", word, ".\nYou guessed it in ",guesses," guesses."
    else:
        print "\nYou lost!\nThe word was", word
    
def Right(wordGuessed, input):
    wordlist = list(word)
    guessedList = list(wordGuessed)
    for i in range(0, len(wordlist)):
        s = wordlist[i]
        if s == input:
            guessedList[i] = input
    wordGuessed = "".join(guessedList)
    return wordGuessed
    
def Wrong(guessesWrong):
    return guessesWrong + 1

def Update(guessesWrong, wordGuessed, guesses):
    print hangmans[guessesWrong]
    print "Your word so far: ", wordGuessed
    print "Guesses: ", guesses
    
    
Intro()
    
while True:
    Update(guessesWrong, wordGuessed, guesses)
    
    while True: #Input Loop
        input = raw_input("Give me a letter! ")
        input = input.lower()
        if len(input) > 1:
            print "One letter at a time, please!"
        elif input in guesses:
            print "You already guessed that letter!"
        else:
            guesses.append(input)
            break
            
    correct = False
    for l in letters: 
        if input == l:
            correct = True

    if correct:
        wordGuessed = Right(wordGuessed, input)
    else:
        guessesWrong = Wrong(guessesWrong)
    
    guessContainsBlank = False
    for s in list(wordGuessed):
        if s == "_":
            guessContainsBlank = True
    
    if guessesWrong == 7:
        GameOver(False, word, guessesWrong)
        break
    elif guessContainsBlank == False:
        GameOver(True, word, guessesWrong + 1)
        break